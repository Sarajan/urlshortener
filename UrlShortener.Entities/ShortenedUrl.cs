﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UrlShortener.Entities
{
    public class ShortenedUrl
    {
        public int Id { get; set; }
        public string Url { get; set; }
    }
}
