﻿using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using UrlShortener.Core.Services;

namespace UrlShortener.Core.Test
{
    [TestFixture]
    public class ShortenerServiceTest
    {
        [TestFixture]
        public class CreateShortUrlTest
        {
            [Test]
            public async Task GenerateShortendUrlForValidUrl()
            {
               
                var mockedContext = new Mock<UrlShortenerDbContext>();
                var shortenerService = new ShortenerService(mockedContext.Object);

                var fragment = await shortenerService.CreateShortUrl("https://docs.microsoft.com/en-us/dotnet/core/docker/build-container");

                Assert.IsFalse(string.IsNullOrEmpty(fragment));
            }
        }
        
    }
}
