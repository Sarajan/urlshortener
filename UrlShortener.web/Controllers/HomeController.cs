﻿using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using System.Threading.Tasks;
using UrlShortener.Core.Services;
using UrlShortener.Entities;
using UrlShortener.web.Models;

namespace UrlShortener.web.Controllers
{
    public class HomeController : Controller
    {
        private readonly ShortenerService shortenerService;

        public HomeController(ShortenerService shortenService)
        {
            shortenerService = shortenService;
        }

        public async Task<IActionResult> Index(string fragment)
        {
            if (string.IsNullOrEmpty(fragment))
            {
                return View();
            }

            var shortenedUrl = await shortenerService.Resolve(fragment);

            if (shortenedUrl != null)
            {
                return View("Resolve", new ShortenedUrlModel()
                {
                    Url = shortenedUrl.Url,
                    ShortUrl = CreateUrl(fragment),
                });
            }
            else
            {
                return View("NoFound", CreateUrl(fragment));
            }
        }

        [HttpPost]
        public async Task<IActionResult> Create(CreateShortenUrlModel model)
        {
            var fragment = await shortenerService.CreateShortUrl(model.Url);

            return View("Created",
                new ShortenedUrlModel()
                {
                    Url = model.Url,
                    ShortUrl = $"http://{HttpContext.Request.Host}/{fragment}"
                });
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        protected string CreateUrl(string fragment)
        {
            return $"http://{HttpContext.Request.Host}/{fragment}";
        }
    }
}
