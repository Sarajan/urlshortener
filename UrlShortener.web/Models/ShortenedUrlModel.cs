﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UrlShortener.web.Models
{
    public class ShortenedUrlModel
    {
        public string Url { get; set; }
        public string ShortUrl { get; set; }
    }
}
