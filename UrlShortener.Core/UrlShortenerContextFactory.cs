﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using System;
using System.Collections.Generic;
using System.Text;

namespace UrlShortener.Core
{
    public class UrlShortenerContextFactory : IDesignTimeDbContextFactory<UrlShortenerDbContext>
    {
        public UrlShortenerDbContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<UrlShortenerDbContext>();
            var connectionString = "Server=.\\SQLEXPRESS;Database=UrlShortener;Trusted_Connection=True;";
            optionsBuilder.UseSqlServer(connectionString);

            return new UrlShortenerDbContext(optionsBuilder.Options);
        }       
    }
}
