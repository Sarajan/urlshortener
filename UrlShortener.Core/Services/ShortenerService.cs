﻿using Base62;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using UrlShortener.Entities;

namespace UrlShortener.Core.Services
{
    public class ShortenerService
    {
        protected UrlShortenerDbContext _context;
        protected Base62Converter converter = new Base62Converter();

        public ShortenerService()
        {
        }

        public ShortenerService(UrlShortenerDbContext context)
        {
            _context = context;
        }

        public async Task<string> CreateShortUrl(string url)
        {
            var newUrl = new ShortenedUrl() { Url = url };
            _context.Urls.Add(newUrl);
            await _context.SaveChangesAsync();
            //var bytes = BitConverter.GetBytes(newUrl.Id);                 

            return converter.Encode(newUrl.Id.ToString());
        }

        public async Task<ShortenedUrl> Resolve(string fragment)
        {
            if (int.TryParse(converter.Decode(fragment), out int id))
            {
                var shortenedUrl = await _context.Urls.FindAsync(id);
                return shortenedUrl;
            }

            throw new Exception("Invalid url fragment");
        }
    }
}
