﻿using Microsoft.EntityFrameworkCore;
using UrlShortener.Entities;

namespace UrlShortener.Core
{
    public class UrlShortenerDbContext : DbContext
    {
        public DbSet<ShortenedUrl> Urls { get; set; }

        public UrlShortenerDbContext() { }
        public UrlShortenerDbContext(DbContextOptions options) : base(options) {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<ShortenedUrl>(b =>
            {
                b.HasKey(m => m.Id);
                b.Property(m => m.Id).ValueGeneratedOnAdd();
            });
        }
    }
}
